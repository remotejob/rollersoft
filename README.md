# rollersoft

## WORKFLOW

kubectl apply -f k8s/domain/cert-rollersoft-ml.yml
kubectl get certificate  
kubectl get event  
kubectl get challenges  

### Persisten volume
kubectl apply -f k8s/domain/pv-volume.yaml  
kubectl delete -f k8s/domain/pv-volume.yaml  
kubectl get pv  
kubectl apply -f k8s/domain/pv-claim.yaml  
kubectl delete -f k8s/domain/pv-claim.yaml
kubectl get pvc

### DEMOSITE
kubectl apply -f k8s/domain/redirect-https.yml  
kubectl apply -f k8s/domain/rollersoft-ml.yml  
kubectl delete -f k8s/domain/rollersoft-ml.yml  
kubectl rollout restart deployment rollersoft-ml-depl -n webs-dev

### Sources
https://gitlab.com/remotejob/typescriptdemosite  
https://gitlab.com/gitlab.com/remotejob/k8staticservergolang  
https://gitlab.com/remotejob/k8staticserverrust  
https://hub.docker.com/r/remotejob/k8staticservergolang/tags  
https://hub.docker.com/r/remotejob/k8staticserverrust/tags  
https://gitlab.com/remotejob/rollersoft  
